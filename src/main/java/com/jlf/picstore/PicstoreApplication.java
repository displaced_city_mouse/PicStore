package com.jlf.picstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PicstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(PicstoreApplication.class, args);
	}

}
