package com.jlf.picstore.models;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.relational.core.mapping.Table;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "model_kit_info")
public class ModelKitInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String model_name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private ModelScale scale;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Manufacturer manufacturer;

    @Lob
    private byte[] image;

}
